<?php

namespace App\Model;
 
class Observer {
    public $text = 'Aucune';
    public $cpt;

    public function update() {
        $this->cpt++;
        $this->text = strval($this->cpt)." fois";
    }

    public function display() {
        return $this->text;
    }
}