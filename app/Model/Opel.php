<?php
namespace App\Model;
use App\Model\Template;

class Opel extends Template {
    private $tva = "10%";

    public function getTVA(){
        return $this->tva;
    }

    public function getMarque(){
        return 'Opel';
    }

    public function getOptions(){
        return 'automatique';
    }
}