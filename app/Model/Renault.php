<?php
namespace App\Model;

use App\Model\Template;

class Renault extends Template {
    private $tva = "20%";
    public function getTVA(){
        return $this->tva;
    }

    public function getMarque(){
        return 'Renault';
    }

    public function getOptions(){
        return '4x4';
    }
}