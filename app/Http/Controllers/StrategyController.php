<?php
namespace App\Http\Controllers;

use App\Models\classStrategy\FactureLine;
use Illuminate\Routing\Controller;

class StrategyController extends Controller
{
    function affichage() {
        $factureOpel = new FactureLine('Opel');
        $factureRenault = new FactureLine('Renault');
        return view('factureline', ['opel' => $factureOpel, 'renault' => $factureRenault]);
    }
}