<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Model\Commande;

class CommandeController extends Controller
{
    public function show()
    {
        $commande = new Commande("opel", "corsa");

        return view('commande', [
            'commande' => $commande
        ]);
    }
}