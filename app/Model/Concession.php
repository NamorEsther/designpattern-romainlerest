<?php

namespace App\Model;

class Concession {
    public $voitures;
    public $nom;

    public function __construct($nomConcession) {
        $this->nom = $nomConcession;
        $this->voitures = [];
    }

    public function addVoiture($une_voiture) {
        array_push($this->voitures, $une_voiture);
    }

    public function getVoiture() {
        return $this->voitures;
    }
}