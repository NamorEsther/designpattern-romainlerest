<?php

namespace App\Model;

class Aggregation {
    public $datas = [];

    public function __construct($voituresConcession) {
        foreach ($voituresConcession as $voiture) {
            $this->datas[] = $voiture->getMakeAndModel();
        }
    }

    public function iterator() {
        $finalValue = '';

        foreach ($this->datas as $value) {
            $finalValue = $finalValue.''.$value;
            $finalValue = $finalValue.', ';
        }

        return $finalValue;
    }
}

