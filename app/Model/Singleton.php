<?php

namespace App\Model;

class Singleton
{
    /**
     * @var int
     */
    private $cpt = 0;

    /**
     * @var array
     */
    private $instance = [];

    private static $singleton;

    private function __construct() {

    }

    public static function getInstance() {
        if (!isset(self::$singleton)) {
            self::$singleton = new Singleton();
        }
        return self::$singleton;
    }

    public function increment() {
        $this->cpt = $this->cpt + 1;
        return $this->cpt;
    }

    public function value() {
        return $this->cpt;
    }
}
