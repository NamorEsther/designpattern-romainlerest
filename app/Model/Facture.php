<?php

namespace App\Model;

class Facture {
    public $montant;

    public function __construct() {
        $this->montant = 0;
    }

    public function addMontantVoiture($voiture) {
        $this->montant = $this->montant + $voiture->getPrix();
    }

    public function getMontant() {
        return $this->montant;
    }
}