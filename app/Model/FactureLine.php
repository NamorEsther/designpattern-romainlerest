<?php

namespace App\Model;

use App\Model\Opel;
use App\Model\Renault;

class FactureLine {
    private $strategy = null;
    public function __construct($voiture){
        switch ($voiture) {
            case "Opel":
                $this->strategy = new Opel();
            break;
            case "Renault":
                $this->strategy = new Renault();
            break;
            default:
            dd('erreur');
        }
    }
    public function getStrategy(){
        return $this->strategy->getTVA();
    }
}