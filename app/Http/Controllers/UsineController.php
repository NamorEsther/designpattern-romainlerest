<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Model\Usine;

class UsineController extends Controller
{
    /**
     * Show the profile for the given user.
     *
     * @return View
     */
    public function show()
    {
        $opel = Usine::fabrication("opel", "corsa");
        $renault = Usine::fabrication("renault", "twingo");

        return view('voiture', [
            'opel' => $opel,
            'renault' => $renault
            ]);
    }
}