<?php

namespace App\Model;
 
class Subject {
    public $observers;

    public function attach($observer) {
        $this->observers[] = $observer;
    }

    public function delete($nomObserver) {
        unset($observers[$nomObserver]);
        $observers = array_values($observers);
    }

    public function notify($observer) {
        return $this->observers;
    }
}