<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/test', 'TestController@show');
Route::get('/singleton', 'SingletonController@show');
Route::get('/usine', 'UsineController@show');
Route::get('/commande', 'CommandeController@show');
Route::redirect('/test2', 'test');
