<html>
<body>
    <header>
        <h1 class="display-1">Commande</h1>
    </header>
    <main>
        <p>{{ $commande->voiture->getMakeAndModel() }}</p>
        <p>{{ $commande->facture->getMontant() }} €</p>
        <p>{{ $commande->concession->nom }}</p>
        <p>Dans la concession, il y a : {{ $commande->aggregation->iterator() }}</p>
    </main>
</body>
</html>