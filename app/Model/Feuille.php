<?php
namespace App\Model;

use App\Model\Composite;

class Feuille extends Composite {
    private $price;

    public function __construct($price){
        $this->price = $price;
    }
    
    public function getPrice() {
        return $this->price;
    }
}