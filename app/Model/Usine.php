<?php

namespace App\Model;

class Automobile
{
    private $vehicleMake;
    private $vehicleModel;

    public function __construct($make, $model)
    {
        $this->vehicleMake = $make;
        $this->vehicleModel = $model;
    }

    public function getMakeAndModel()
    {
        return $this->vehicleMake . ' ' . $this->vehicleModel;
    }

    public function getPrix() {
        return rand(10000, 20000);
    }
}

class Usine
{
    public static function fabrication($make, $model)
    {
        return new Automobile($make, $model);
    }
}