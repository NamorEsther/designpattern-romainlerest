<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Model\Singleton;

class SingletonController extends Controller
{
    /**
     * Show the profile for the given user.
     *
     * @return View
     */
    public function show()
    {
        $a = Singleton::getInstance();
        $b = Singleton::getInstance();

        return view('singleton', [
            'singleton1' => $a,
            'singleton2' => $b
            ]);
    }
}