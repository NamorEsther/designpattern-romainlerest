<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\classTemplate\Opel;
use App\Models\classTemplate\Renault;

class TemplateController extends Controller {
    function affichage() {
        $opel = new Opel();
        $renault = new Renault();
        return view('template', ['opel' => $opel, 'renault' => $renault]);
    }
}