<?php

namespace App\Model;

use App\Model\Facture;
use App\Model\Usine;
use App\Model\Concession;
use App\Model\Aggregation;

class Commande {
    public $voiture;
    public $concession;
    public $facture;
    public $voiture2;
    public $aggregation;

    public function __construct($make, $model) {
        $this->voiture = Usine::fabrication($make, $model);
        $this->voiture2 = Usine::fabrication("renault", "megane");
        $this->concession = new Concession("Garage Simon");
        $this->concession->addVoiture($this->voiture);
        $this->concession->addVoiture($this->voiture2);
        $this->facture = new Facture();
        $this->facture->addMontantVoiture($this->voiture);
        $this->facture->addMontantVoiture($this->voiture2);
        $this->aggregation = new Aggregation($this->concession->getVoiture());
    }
}