<html>
<body>
    <header>
        <h1 class="display-1">Singletons</h1>
    </header>
    <main>
        <p>{{ $singleton1->increment() }}</p>
        <p>{{ $singleton2->increment() }}</p>
        <p>{{ $singleton1->increment() }}</p>
    </main>
</body>
</html>